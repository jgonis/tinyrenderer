class AppMain {

    constructor(context: Window) {
        this.m_context = context;
        this.m_canvas = <HTMLCanvasElement>(this.m_context.document.getElementById("canvas"));

        this.m_worker = new Worker("build/worker.js");
        this.SetupEventHandlers();
        this.m_context.setTimeout(() => {
            this.HandleCanvasResize();
        }, 200);
    }

    private SetupEventHandlers() {
        console.log("setting up events");
        this.m_worker.addEventListener("message", (ev: MessageEvent) => {
            console.log("worker message received");
        });

        this.m_context.window.addEventListener("resize", (event: UIEvent) => {
            this.WindowResize(event);
        });

        this.m_canvas.addEventListener("drop", (ev: DragEvent) => {
            let length: number = ev.dataTransfer.files.length;
            console.log(length);
            this.FileDropped(ev);
        })

        this.m_canvas.addEventListener("dragenter", (ev: DragEvent)=> {
            ev.stopPropagation();
            ev.preventDefault();
        });
        this.m_canvas.addEventListener("dragover", (ev: DragEvent)=> {
            ev.stopPropagation();
            ev.preventDefault();
        });
    }

    private FileDropped(ev: DragEvent) {
        ev.stopPropagation();
        ev.preventDefault();

        let fl: FileList = ev.dataTransfer.files;
        for(let i = 0; i < fl.length; i++) {
            let file: File = fl[i];
            this.m_worker.postMessage({"type": "File", "payload": file});
        }
    }

    private HandleCanvasResize() {
        let dpr = this.m_context.window.devicePixelRatio;
        let cWidth = Math.floor(this.m_canvas.clientWidth * dpr);
        let cHeight = Math.floor(this.m_canvas.clientHeight * dpr);
        if (cWidth !== this.m_canvasWidth || cHeight !== this.m_canvasHeight) {
            this.m_canvas.width = cWidth;
            this.m_canvasWidth = cWidth;
            this.m_canvas.height = cHeight;
            this.m_canvasHeight = cHeight;
        }
        let ctx: CanvasRenderingContext2D | null = this.m_canvas.getContext("2d");
        if(ctx !== null) {
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.fillRect(0, 0, this.m_canvas.width, this.m_canvas.height);
            console.log("drawing");
        }
    }

    private WindowResize(event: UIEvent) {
        this.m_context.requestAnimationFrame(() => {
            this.HandleCanvasResize();
        });
    }

    private m_context: Window;
    private m_canvas: HTMLCanvasElement;
    private m_worker: Worker;
    private m_canvasHeight: number = -1;
    private m_canvasWidth: number = -1;
}


((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);
