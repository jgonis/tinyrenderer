export class FileDroppedHandler {
	constructor() {
	}

	public handleDrop(ev: DragEvent) {
			ev.stopPropagation();
			ev.preventDefault();

			let fl: FileList = ev.dataTransfer.files;
			for(let i = 0; i < fl.length; i++) {
				let file: File = fl[i];
				let fr: FileReader = new FileReader();
				fr.addEventListener("load", (ev: UIEvent) => {
					let str: string = fr.result;
					console.log(str);
				})
				fr.readAsText(file);
			}
	}

	public testMethod() {
		console.log("testing");
	}
}